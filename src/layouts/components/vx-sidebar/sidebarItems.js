/*=========================================================================================
  File Name: sidebarItems.js
  Description: Sidebar Items list. Add / Remove menu items from here.
  Strucutre:
          url     => router path
          name    => name to display in sidebar
          slug    => router path name
          icon    => Feather Icon component/icon name
          tag     => text to display on badge
          tagColor  => class to apply on badge element
          i18n    => Internationalization
          submenu   => submenu of current item (current item will become dropdown )
                NOTE: Submenu don't have any icon(you can add icon if u want to display)
          isDisabled  => disable sidebar item/group
  ----------------------------------------------------------------------------------------
  Item Name: Vuesax Admin - VueJS Dashboard Admin Template
  Author: Pixinvent
  Author URL: http://www.themeforest.net/user/pixinvent
==========================================================================================*/


export default [
  {
   
      
    
  },
  {
    header: "Apps",
    i18n: "Apps",
  },
  
  {
    url: "/apps/chat",
    name: "Chat",
    slug: "chat",
    icon: "MessageSquareIcon",
    i18n: "Chat",
  },
  {
    url: "/apps/todo",
    name: "Todo",
    slug: "todo",
    icon: "CheckSquareIcon",
    i18n: "Listas",
  },
  {
    url: "/apps/calendar/vue-simple-calendar",
    // url: null,
    name: "Calendar",
    slug: "calendar",
    icon: "CalendarIcon",
    tag: "new",
    tagColor: "success",
    i18n: "Calendario",
    // submenu: [
    //   {
    //     url: '/apps/calendar/vue-simple-calendar',
    //     name: "Simple Calendar",
    //     slug: "calendarSimpleCalendar",
    //     i18n: "SimpleCalendar",
    //   },
    //   {
    //     url: '/apps/calendar/vue-fullcalendar',
    //     name: "Full Calendar",
    //     slug: "calendarFullCalendar",
    //     i18n: "FullCalendar",
    //   }
    // ]
  },
  {
    url: null,
    name: "eCommerce",
    slug: "eCommerce",
    icon: "ShoppingCartIcon",
    i18n: "eCommerce",
    submenu: [
      {
        url: '/apps/eCommerce/shop',
        name: "Shop",
        slug: "eCommerceShop",
        i18n: "Productos",
      },
      {
          url: '/apps/eCommerce/wish-list',
          name: "Wish List",
          slug: "eCommerceWishList",
          i18n: "Deseados",
      },
      {
        url: '/apps/eCommerce/checkout',
        name: "Checkout",
        slug: "eCommerceCheckout",
        i18n: "Checkout",
      },
    ]
 
    
  },
  
  {
    header: "Pages",
    i18n: "Paginas",
  },
  {
    url: '/pages/profile',
    name: "Profile",
    icon: "UserIcon",
    i18n: "Perfil",
  },
  {
    url: '/pages/faq',
    name: "FAQ",
    icon: "HelpCircleIcon",
    i18n: "Preguntas Frecuentes",
  },
  {
    url: '/pages/knowledge-base',
    name: "Knowledge Base",
    icon: "InfoIcon",
    i18n: "Sobre Nosotros",
  },
  {
    url: '/pages/search',
    name: "Search",
    icon: "SearchIcon",
    i18n: "Busqueda",
  },
 
  {
    url: null,
    name: "Authentication",
    icon: "PieChartIcon",
    i18n: "Seguridad",
    submenu: [
      {
        url: '/pages/login',
        name: "Login",
        slug: "pagesLogin",
        i18n: "Login",
        target: '_blank',
      },
      {
        url: '/pages/register',
        name: "Register",
        slug: "pagesRegister",
        i18n: "Registrarse",
        target: '_blank',
      },
      {
        url: '/pages/forgot-password',
        name: "Forgot Password",
        slug: "pagesForgotPassword",
        i18n: "Contraseña Olvidada",
        target: '_blank',
      },
      {
        url: '/pages/reset-password',
        name: "Reset Password",
        slug: "pagesResetPassword",
        i18n: "Reestablecer Contraseña",
        target: '_blank',
      },
    
    ]
  },
  {
    url: null,
    name: "Miscellaneous",
    icon: "CoffeeIcon",
    i18n: "Extras",
    submenu: [
        {
          url: '/pages/not-authorized',
          name: "Not Authorized",
          slug: "pageNotAuthorized",
          icon: "XCircleIcon",
          i18n: "SinPermiso",
          target: '_blank',
        },
        {
          url: '/pages/maintenance',
          name: "Maintenance",
          slug: "pageMaintenance",
          icon: "AnchorIcon",
          i18n: "Mantenimiento",
          target: '_blank',
        },
        {
          url: '/pages/comingsoon',
          name: "Coming Soon",
          icon: "ClockIcon",
          i18n: "Muy Pronto",
          target: '_blank',
        },
        {
          url: '/pages/error-404',
          name: "404",
          slug: "pageError404",
          i18n: "404",
          target: '_blank',
        },
        {
          url: '/pages/error-500',
          name: "500",
          slug: "pageError500",
          i18n: "500",
          target: '_blank',
        }
    ]
  },
 
  
  

]
